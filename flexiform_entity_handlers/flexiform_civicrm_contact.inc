<?php

/**
 * {@inheritdoc}
 */
class FlexiformCiviCrmContact extends FlexiformFormEntityLegacy {

  function __construct(FlexiformFormEntityManagerInterface $manager, $namespace, $getter, $settings = array()) {
    parent::__construct($manager, $namespace, $getter, $settings);
    $this->settings += $settings;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntity() {
    $base_entity = $this->manager->getBaseEntity();
    // Do we have a contact or Drupal user as the base entity?
    if(isset($base_entity->uid)) {
      // We have a Drupal user entity as the base entity, get the contact for it.

      $cid = CRM_Core_BAO_UFMatch::getContactId($base_entity->uid);
      $contact = entity_load('civicrm_contact', array($cid));
      return reset($contact);
    }
    else {
      // We already have the contact entity, return it.
      return $base_entity;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function configForm($form, &$form_state) {
    return $form;
  }

  public function saveEntity($entity) {
    entity_save('civicrm_contact', $entity);
  }

}
