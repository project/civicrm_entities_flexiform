<?php

class CiviCrmFlexiformStateElement extends CiviCrmFlexiformSelectElement {


  /**
   * Return the form element for this FlexiformElement.
   */
  public function form($form, &$form_state, $entity, $language = LANGUAGE_NONE) {
    $form = parent::form($form, $form_state, $entity);

    // Because we're inheriting, let the parent class do the initial work.
    $select = &$form[$this->element_namespace];

    // Reset the #options each time because it is dependant on the country_id.
    $select['#options'] = array();

    if (empty($country_id) && !empty($form_state['values']['civicrm_address']['country_id'])) {
      $country_id = $form_state['values']['civicrm_address']['country_id'];
    }
    else {
      // Filter down the options to the select country.
      $country_id = $entity->country_id;
    }

    if ($country_id) {
      $states_provinces = CRM_Core_PseudoConstant::stateProvinceForCountry($country_id);
      // Does this country have states/provinces?
      if(!empty($states_provinces)) {
        // Sort alphabetically.
        asort($states_provinces);
        $select['#options'] = (array('' => t('- Select -')) + $states_provinces);
        $select['#attributes']['data-value'] = $select['#default_value'];
      }
    }
    else {
      $select['#options'] = array();
    }

    $select['#attributes']['class'][] = 'state-id';
    $select['#prefix'] = '<div id="state-province-wrapper">';
    $select['#suffix'] = '</div>';
    $select['#required'] = !empty($select['#options']);

    return $form;
  }

  /**
   * Validate the form element.
   */
  public function formValidate($form, &$form_state, $entity, $language = LANGUAGE_NONE) {
    parent::formValidate($form, $form_state, $entity, $language);
  }

  /**
   * Submit the form element.
   */
  public function formSubmit($form, &$form_state, $entity, $language = LANGUAGE_NONE) {
    parent::formSubmit($form, $form_state, $entity, $language);
  }

  /**
   * Extract the submitted values for this form element.
   */
  public function formExtractValues($form, &$form_state, $entity) {
    $value = parent::formExtractValues($form, $form_state, $entity);
    // Concat with a string so null values are actually empty strings.
    // there are times when a country has not states/provinces so this scenario of an empty string applies.
    // Null values will skip the overwrite/update process.
    return $value . "";
  }

  /**
   * {@inheritdoc}
   */
  public function configureForm($form, &$form_state, $flexiform) {
    $form = parent::configureForm($form, $form_state, $flexiform);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configureFormSubmit($form, &$form_state, $flexiform) {

    parent::configureFormSubmit($form, $form_state, $flexiform);
  }

}
