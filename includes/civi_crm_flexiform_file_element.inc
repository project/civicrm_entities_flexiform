<?php

class CiviCrmFlexiformFileElement extends CiviCrmEntitiesFlexiformElementBase {


  /**
   * Return the form element for this FlexiformElement.
   */
  public function form($form, &$form_state, $entity, $language = LANGUAGE_NONE) {
    $parents = $form['#parents'];
    $parents[] = $this->element_info['name'];

    // Work out the default value.
    $file = null;

    $default = self::getDefaultValue($form, $entity);

    if(isset($default['fid'])) {
      $file = self::getFilebyId($default['fid'], $entity);
    }


    $element = array(
      '#type' => 'managed_file',
      '#parents' => $parents,
      '#title' => $this->label(),
      '#upload_location' => self::getUploadPath(),
      '#upload_validators' => array(
        'file_validate_extensions' => array(
          $this->settings['file_extensions'],
        ),
      ),
      '#default_value' => ($file ? $file->fid : null),
    );

    $element = self::defaultElementInfo($element, $form, $entity);
    $form[$this->element_namespace] = $element;

    $form = parent::form($form, $form_state, $entity);

    return $form;
  }

  /**
   * Validate the form element.
   */
  public function formValidate($form, &$form_state, $entity, $language = LANGUAGE_NONE) {
    parent::formValidate($form, $form_state, $entity, $language);
  }

  /**
   * Submit the form element.
   */
  public function formSubmit($form, &$form_state, $entity, $language = LANGUAGE_NONE) {
    parent::formSubmit($form, $form_state, $entity, $language);
  }

  private static function getFilebyId($file_id, $entity) {
    $wrapper = file_stream_wrapper_get_instance_by_uri('public://');
    $public_path = $wrapper->realpath();
    
    $entityId = $entity->id;
    list($file_src) = CRM_Core_BAO_File::path($file_id, $entityId,
                NULL, NULL
              );    
    
    $uri = str_replace($public_path, 'public:/', $file_src);
    $files = file_load_multiple(array(), array('uri' => $uri));
    if(!empty($files)) {
      return reset($files);
    }
    return null;
  }
  
/*  private static function getFileIdFromUri($drupal_uri) {
    //Check if the file name exsits in the civi database
    $file_path = drupal_realpath($drupal_uri);
    $wrapper = file_stream_wrapper_get_instance_by_uri('public://');
    $public_path = $wrapper->realpath();
    $private_wrapper = file_stream_wrapper_get_instance_by_uri('private://');
    $private_path = $wrapper->realpath();
    $file_name = str_replace($public_path . '/', '', $file_path);
    $file_name = str_replace($private_path . '/', '', $file_name);
    // Get the name of the
    $name = db_select('civicrm_file', 'f')
                ->fields('f', array('file_type_id'))
                ->condition('uri', $file_name, '=')
                ->execute()
                ->fetchField();  
    
    return $file_id;
    
  }*/

  /**
   * Extract the submitted values for this form element.
   */
  public function formExtractValues($form, &$form_state, $entity) {
    $value = parent::formExtractValues($form, $form_state, $entity);
    $uid = CRM_Core_BAO_UFMatch::getUFId($entity->id);
    if(empty($value['fid'])) {
      // If the original file was removed, we need to tell Drupal about the change so the cron will clean it up.
      // Check if originally there was a file and if the current upload is empty.
      if(isset($entity->{$this->element_info['name']})) {
        $this->removeFile($entity);
      }

      return '';
    }
    // Check if nothing has changed.
    elseif($form[$this->element_namespace]['#default_value'] == $value['fid']) {
      $file_id = $entity->{$this->element_info['name']};
      $file_id = $file_id['fid'];
      $file = self::getFilebyId($file_id, $entity);
      return self::getCiviValuesFromFile($file);
    }

    // We have a new uploaded file, continue to save its usage and return the path to Flexiform.

    
    // Set the file's status so the cron won't clean it up thinking it was never used.
    $file = file_load($value['fid']);
    $file->status = FILE_STATUS_PERMANENT;
    file_save($file);
    file_usage_add($file, 'civicrm_entities_flexiform', 'user', $uid);
    

    return self::getCiviValuesFromFile($file);
  }

  public static function getCiviValuesFromFile($file) {   
    $file_path = drupal_realpath($file->uri); 
    $civi_values = array('name' => $file_path, 'type' => $file->filemime);
    
    return $civi_values;
  }
  
  public function insertCiviEntityFile($fileId, $entity) {
 
    $groupId = $this->element_info['custom_group_id'];
    $groupName = self::getGroupName($groupId);
    $entity_table = 'civicrm_value_'. strtolower($groupName). '_'. $groupId;
       
    //Creates a record in the civicrm_file table for this $file
    $civiFileEntityId = db_insert('civicrm_entity_file') // Table name no longer needs {}
    ->fields(array(
      'entity_table' => $entity_table,
      'entity_id' => $entity->id,
      'file_id' => $fileId,  
    ))
    ->execute();    
    
    return intval($civiFileEntityId);
  }  
  
  private static function getGroupName($groupId) {
    // Get the name of the civicrm custom group from the group id
    $name = db_select('civicrm_custom_group', 'g')
                ->fields('g', array('name'))
                ->condition('id', $groupId, '=')
                ->execute()
                ->fetchField();  
    
    return $name;
  }
  
  private function removeFile($entity) {
    $file_id = $entity->{$this->element_info['name']};
    // At this point, the $file_id is the civicrm_file ID.
    $file_id = $file_id['fid'];
    
    if (!empty($file_id)) {
      $file = self::getFilebyId($file_id, $entity);
      //was the file manually removed?
      if (!empty($file)) {
        $uid = CRM_Core_BAO_UFMatch::getUFId($entity->id);
        file_usage_delete($file, 'civicrm_entities_flexiform', 'user', $uid);
        $file->status = 0;
        file_save($file);        
        
        // We're going to delete the references to the file in Civi's tables manually (directly at the DB level).
        // We'll do this because Civi physically removes the file while Drupal waits on the cron and evaluates the file usage table for pending removals.
        // Because we also have a Drupal file usage reference to this file, we'll let Drupal take care of the deleting work.
        db_delete('civicrm_entity_file')->condition('file_id', $file_id)->execute();
        db_delete('civicrm_file')->condition('id', $file_id)->execute();
      }
    }
    
   
  }

  /**
   * {@inheritdoc}
   */
  public function configureForm($form, &$form_state, $flexiform) {
    $form = parent::configureForm($form, $form_state, $flexiform);

    $form['file_extensions'] = array(
      '#type' => 'textfield',
      '#title' => 'File types',
      '#default_value' => $this->settings['file_extensions'],
      '#description' => 'Enter the allowed file extensions separated by a space.',
    );

    $path = self::getUploadPath();
    // Add some markup showing the current destination path and mentioning that it is configured in CiviCRM's path config.
    $form['path'] = array(
      '#markup' => '<div>
          <p>The upload path is controlled by CiviCRM.<br />
          <b>Current path:</b> <pre>' . $path . '</pre></p>

          <p>You can change this location in CiviCRM\'s <a href="/civicrm/admin/setting/path?reset=1">Upload Directories settings</a> page.</p>
        </div>',
    );


    return $form;
  }

  /**
   * Returns the upload path set in the CiviCRM config relative to the site's root.
   *
   * @return string
   */
  public static function getUploadPath() {
    civicrm_initialize();
    $config = CRM_Core_Config::singleton();
    $civi_file_dir = $config->customFileUploadDir;
    $public_path = file_stream_wrapper_get_instance_by_uri('public://')->realpath();
    $private_path = file_stream_wrapper_get_instance_by_uri('private://')->realpath();

    // Is this in a public file scheme?
    if(stripos($civi_file_dir, $public_path) !== false) {
      $civi_file_dir = str_ireplace($public_path, 'public:/', $civi_file_dir);
    }
    // Is this in a private file scheme?
    elseif(stripos($civi_file_dir, $private_path, $civi_file_dir) !== false) {
      $civi_file_dir = str_ireplace($public_path, 'private:/', $civi_file_dir);
    }
    else {
      // The current config does match a Drupal file scheme.
      return null;
    }

    return $civi_file_dir;
  }

  /**
   * {@inheritdoc}
   */
  public function configureFormSubmit($form, &$form_state, $flexiform) {

    $this->settings['file_extensions'] = $form_state['values']['file_extensions'];
    $flexiform->updateElement($this);
    $flexiform->save();

    parent::configureFormSubmit($form, $form_state, $flexiform);
  }

  public static function processEntityFileProperties(&$entities, $entity_name) {
    $name = $entity_name;
    // We need to convert "contact" to individual for some stupid reason.
    $name = str_ireplace('contact', 'individual', $name);
    // Civi wants the casing to be correct so we'll capitalize the first letter.
    $name = ucwords($name, "  _");
    $custom_fields = CRM_Core_BAO_CustomField::getFields($name);
    foreach($custom_fields as $cid => $field) {
      if($field['data_type'] == 'File') {
        // Loop over the entities to update the file fields in each.
        foreach($entities as &$entity) {
          // retrieve the fid if applicable.
          $fid = $entity->{"custom_$cid"};
          if(is_numeric($fid)) {
            $field_group = reset(\CRM_Core_BAO_CustomGroup::getGroupDetail($field['custom_group_id']));
            $files = \CRM_Core_BAO_File::getEntityFile($field_group['table_name'], $entity->id);
            if(array_key_exists($fid, $files)) {
              $file = $files[$fid];

              // Load file details from civi's file ID.
              $file_object = array(
                'name' => $file['fullPath'],
                'type' => $file['mime_type'],
                'fid' => $fid,
              );

              $entity->{"custom_$cid"} = $file_object;
            }
          }
        }
      }
    }
  }

  /**
   * Gets the potential default value for this element.
   *
   * @param array $form Drupal form array.
   * @param object $entity The entity that this element is used for.
   * @return string
   */
  protected function getDefaultValue($form, $entity) {
    $default = null;

    // Should we load an initial value from an existing entity?
    if ($entity->id && isset($entity->{$this->element_info['name']})) {
      $default = $entity->{$this->element_info['name']};
    }
    elseif (!empty($this->settings['default_value']['default_value'])) {
      $default = $this->settings['default_value']['default_value'];
    }

    return $default;
  }

}
