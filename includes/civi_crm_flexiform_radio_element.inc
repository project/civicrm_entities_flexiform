<?php

class CiviCrmFlexiformRadioElement extends CiviCrmEntitiesFlexiformElementBase {


  /**
   * Return the form element for this FlexiformElement.
   */
  public function form($form, &$form_state, $entity, $language = LANGUAGE_NONE) {

    // If the data type if boolean, then use default options:
    if($this->element_info['type_name'] == 'Boolean') {
      $options = array(
        0 => t('No'),
        1 => t('Yes'),
      );
    }
    else {
      // Get the custom value components.
      $options = CRM_Core_OptionGroup::valuesByID($this->element_info['option_group_id']);
    }


    $element = array(
      '#type' => 'radios',
      '#options' => $options,
    );

    $element = self::defaultElementInfo($element, $form, $entity);
    $form[$this->element_namespace] = $element;

    $form = parent::form($form, $form_state, $entity);

    return $form;
  }

  /**
   * Validate the form element.
   */
  public function formValidate($form, &$form_state, $entity, $language = LANGUAGE_NONE) {
    parent::formValidate($form, $form_state, $entity, $language);
  }

  /**
   * Submit the form element.
   */
  public function formSubmit($form, &$form_state, $entity, $language = LANGUAGE_NONE) {
    parent::formSubmit($form, $form_state, $entity, $language);
  }

  /**
   * Extract the submitted values for this form element.
   */
  public function formExtractValues($form, &$form_state, $entity) {
    $value = parent::formExtractValues($form, $form_state, $entity);
    return $value;
  }

  /**
   * {@inheritdoc}
   */
  public function configureForm($form, &$form_state, $flexiform) {
    $form = parent::configureForm($form, $form_state, $flexiform);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function configureFormSubmit($form, &$form_state, $flexiform) {

    parent::configureFormSubmit($form, $form_state, $flexiform);
  }

  /**
   * Gets the potential default value for this element.
   *
   * @param array $form Drupal form array.
   * @param object $entity The entity that this element is used for.
   * @return string
   */
  protected function getDefaultValue($form, $entity) {

    $default = parent::getDefaultValue($form, $entity);

    // Work out the default value.
    if (!isset($default) && !empty($this->settings['default_value']['default_value'])) {
      $default = $this->settings['default_value']['default_value'];
    }
    if (!empty($this->settings['default_value']['use_tokens'])) {
      $default = $this->replaceCtoolsSubstitutions($default, $form['#flexiform_entities']);
    }

    // Should we load an initial value from an existing entity?
    if ($entity->id && isset($entity->{$this->element_info['name']})) {
      $default = $entity->{$this->element_info['name']};
    }

    return $default;
  }
}
